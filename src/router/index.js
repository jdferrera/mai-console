import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Intent from '@/components/Intent/Intent'
import Topic from '@/components/Topic/Topic'
import TopicIntent from '@/components/Topic/TopicIntent'
import CreateIntent from '@/components/Intent/CreateIntent'
import Chat from '@/components/Chat/Chat'

import Service from '@/components/Service/Service'
import Variable from '@/components/Variable/Variable'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/home',
      name: 'Home',
      component: Home
    },
    {
      path: '/intent/intent',
      name: 'Intent',
      component: Intent
    },
    {
      path: '/intent/intent/createintent',
      name: 'CreateIntent',
      component: CreateIntent
    },
    {
      path: '/variable/variable',
      name: 'Variable',
      component: Variable
    },
    {
      path: '/topic/topic',
      name: 'Topic',
      component: Topic
    },
    {
      path: '/topic/topic/intent',
      name: 'TopicIntent',
      component: TopicIntent
    },
    {
      path: '/topic/topic/createintent',
      name: 'CreateIntentTopic',
      component: CreateIntent
    },
    {
      path: '/service/service',
      name: 'Service',
      component: Service
    },
    {
      path: '/chat/chat',
      name: 'Chat',
      component: Chat
    }
  ]
})
