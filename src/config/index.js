export default {
  topicURI: 'https://localhost:8444/topics',
  variableURI: 'https://localhost:8444/userdefined/variables',
  variableBuiltInURI: 'https://localhost:8444/builtin/variables',
  servicesURI: 'https://localhost:8444/servicestemplate',
  variableAccessToken: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJTQ09QRV9DSEFUIiwiU0NPUEVfQVBJIl0sImV4cCI6MTUyMzM2NTA2MCwiYXV0aG9yaXRpZXMiOlsiU0NPUEVfQVBJIiwiU0NPUEVfQ0hBVCJdLCJqdGkiOiIxZWEwYzRjZS1iODg0LTQ4NzktOWI5Yy1lZWEyNGRhMGNiZTEiLCJjbGllbnRfaWQiOiJsb2NhbC1jbGllbnQifQ.BwDV3xjwh-TFygTyoQFtsqWLsIMC-KbLsECeUIi5BEo',
  aimlURI: 'https://localhost:8444/aiml'
}
